import { NgxPerfLoggerExamplePage } from './app.po';

describe('ngx-perf-logger-example App', () => {
  let page: NgxPerfLoggerExamplePage;

  beforeEach(() => {
    page = new NgxPerfLoggerExamplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
