import { Component, OnInit, AfterViewInit  } from '@angular/core';
import { LogClassPerformance,
  LogFunctionPerformance,
  PerfLogManager,
  IFlatLog,
  DisableLogFunctionPerformance } from 'ts-perf-logger';
import { AppComponent } from '../../app.component';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';


@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.scss']
})
export class Page1Component implements OnInit, AfterViewInit {
  readonly ITERATIONS = 1000000;

  data: number[];

  constructor() {
    PerfLogManager.logPerfInit('page1_load');
  }

  @LogFunctionPerformance(null, AppComponent.newLogMethod)
  ngOnInit() {
    const ac = new AnotherClass();
    ac.foo();
    ac.bar();
  }

  ngAfterViewInit() {
    PerfLogManager.logPerfEnd('page1_load', true);
  }

  @LogFunctionPerformance()
  usingDecorator(ok: boolean) {
    PerfLogManager.setActionId(PerfLogManager.getActionId() + 1);
    this.doSomeInnerWork();
    if (!ok) {
      (<any>this).something(); // this will fail has something() doesn't exist...
    }
  }

  usingInitEndCalls(ok: boolean) {
    PerfLogManager.logPerfInit('AppComponent.usingInitEndCalls');
    try {
      this.doWork();
      if (!ok) {
        (<any>this).something(); // this will fail has something() doesn't exist...
      }
      PerfLogManager.logPerfEnd('AppComponent.usingInitEndCalls', true);
    } catch (error) {
      PerfLogManager.logPerfEnd('AppComponent.usingInitEndCalls', false);
    }
  }

  @LogFunctionPerformance('AppComponent.usingDecoratorOnPromise')
  usingDecoratorOnPromise(ok: boolean): Promise<string> {
    return this.promise(ok);
  }

  usingInitEndCallsOnPromise(ok: boolean) {
    PerfLogManager.logPerfInit('AppComponent.usingInitEndCallsOnPromise');
    this.promise(ok).then(() => {
      PerfLogManager.logPerfEnd('AppComponent.usingInitEndCallsOnPromise', true);
    }).catch((error) => {
      PerfLogManager.logPerfEnd('AppComponent.usingInitEndCallsOnPromise', false);
    });
  }

  usingInitEndCallsOnObservable(ok: boolean) {
    PerfLogManager.logPerfInit('AppComponent.usingInitEndCallsOnObservable');
    this.observable(ok).subscribe(
      () => {
        PerfLogManager.logPerfEnd('AppComponent.usingInitEndCallsOnObservable', true);
      }, (error) => {
        PerfLogManager.logPerfEnd('AppComponent.usingInitEndCallsOnObservable', false);
      }
    );
  }

  private promise(ok: boolean): Promise<string> {
    return new Promise((resolve, reject) => {
      this.doWork();
      if (ok) {
        resolve();
      } else {
        reject();
      }
    });
  }

  private observable(ok: boolean): Observable<string> {
    return new Observable((observer: Observer<string>) => {
      this.doWork();
      if (ok) {
        observer.next('');
        observer.complete();
      } else {
        observer.error('error');
      }
    });
  }

  private doWork() {
    // 'AppComponent.usingInitEndCalls.doWork'
    // 'AppComponent.observable', 'doWork'
    for (let i = 0; i < this.ITERATIONS; i++) {
      Math.random();
    }
  }

  @LogFunctionPerformance()
  private doSomeInnerWork() {
    this.doSomeInnerInnerWork();
  }
  @LogFunctionPerformance()
  private doSomeInnerInnerWork() {
    this.doWork();
  }
}




@LogClassPerformance()
// @LogClassPerformance(['foo', 'bar'])
class AnotherClass {
  readonly ITERATIONS = 1000000;

  constructor() {
    this.doWork();
  }

  foo() {
    this.doWork();
  }

  bar() {
    this.doWork();
  }

  @DisableLogFunctionPerformance()
  private doWork() {
    for (let i = 0; i < this.ITERATIONS; i++) {
      Math.random();
    }
  }
}
