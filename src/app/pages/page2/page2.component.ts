import { Component, OnInit, AfterViewInit  } from '@angular/core';
import { LogClassPerformance,
  LogFunctionPerformance,
  PerfLogManager,
  IFlatLog,
  DisableLogFunctionPerformance } from 'ts-perf-logger';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';


@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class Page2Component implements AfterViewInit {
  readonly ITERATIONS = 500;

  data: number[];

  constructor() {
    PerfLogManager.logPerfInit('page2_load', Math.floor(Math.random() * 100));

    this.loadData();
  }

  ngAfterViewInit() {
    PerfLogManager.logPerfEnd('page2_load', true);
  }

  @LogFunctionPerformance()
  loadData() {
    PerfLogManager.setActionId(PerfLogManager.getActionId() + 1);
    this.data = [];
    for (let i = 0; i < this.ITERATIONS; i++) {
      this.data.push(Math.floor(Math.random() * 100));
    }
  }
}
