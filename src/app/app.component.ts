import { Component, OnInit  } from '@angular/core';
import { LogClassPerformance,
  LogFunctionPerformance,
  PerfLogManager,
  IFlatLog,
  DisableLogFunctionPerformance } from 'ts-perf-logger';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly ITERATIONS = 1000000;

  stats: string;
  disableChangeLogMethodButton = false;

  static newLogMethod(name: string, actionId: any, success: boolean, startDate: Date, timeTaken: number): void {
    const status = (success) ? 'YUPPIII' : 'FAIL';
    const message = `----- method ${name};  ActionId: ${actionId}; Status: ${status}; Date: ${startDate}; Time: ${timeTaken}ms.`;
    console.warn(message);
  }

  constructor() {
    PerfLogManager.setActionId(0);

    setInterval(() => {
      this.stats = JSON.stringify(PerfLogManager.getStatistics(), null, 2);
    }, 1000);
  }

  changeLogMethod() {
    PerfLogManager.setLogMethod(AppComponent.newLogMethod);
    this.disableChangeLogMethodButton = true;
    console.log('Log Method changed');
  }


}




